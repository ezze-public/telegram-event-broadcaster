<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

use App\Jobs\SendTelegram;

class rcStart extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rc:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get latest update from telegram.';

    /**
     * Execute the console command.
     *
     * @return boolean
     */
    public function handle()
    {
        Redis::subscribe( ['telegram:send'], function ($emit) {
            logg('rcStart ' .$emit);    
            $emit = json_decode($emit);
            SendTelegram::dispatch($emit->chat_id, $emit->text);
        });   
    }
}
