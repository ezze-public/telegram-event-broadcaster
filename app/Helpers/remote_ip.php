<?php

# 2022-12-07

function remote_ip(){

	if( array_key_exists('HTTP_CF_CONNECTING_IP', $_SERVER) )
		return $_SERVER['HTTP_CF_CONNECTING_IP'];

	if( array_key_exists('HTTP_TUNNEL', $_SERVER) )
		return $_SERVER['HTTP_TUNNEL'];
	
	if( array_key_exists('REMOTE_ADDR', $_SERVER) )
		return $_SERVER['REMOTE_ADDR'];

	return false;
	
}

