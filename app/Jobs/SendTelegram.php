<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Telegram\Bot\Laravel\Facades\Telegram;

class SendTelegram implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $chat_id;
    protected $text;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( int $chat_id, string $text )
    {
        $this->chat_id = $chat_id;
        $this->text = $text;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Telegram::sendMessage([
            'chat_id' => $this->chat_id,
            'text' => $this->text
        ]);
        // logg('Telegram::sendMessage ' .$this->chat_id.' : '.$this->text);
        logg('.');
    }
}
