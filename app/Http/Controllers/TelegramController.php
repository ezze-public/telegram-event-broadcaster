<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Telegram\Bot\Laravel\Facades\Telegram;

use Illuminate\Support\Facades\Redis;

class TelegramController extends Controller
{   

    # receive message
    public function handle()
    {

        $update = Telegram::commandsHandler(true);
        $message = $update->getMessage();

        /*
        {
            "message_id":16,
            "from": {
                "id":75557283,
                "is_bot":false,
                "first_name":"Jalal"
                ,"last_name":"Hoseini"
                ,"username":"nginx503"
            },
            "chat": {
                "id":-4100451681
                ,"title":"Jalal & ezzeEventBroadcaster",
                "type":"group",
                "all_members_are_administrators":true
            },
            "date":1706347592,
            "text":"k"
        }
        */
                
        Redis::publish('telegram:received', json_encode($message));

        // // echo back
        // $chat_id = $message->getChat()->getId();
        // $text = $message->getText();
        // Telegram::sendMessage([
        //     'chat_id' => $chat_id,
        //     'text' => $text
        // ]);

        return response($message, 200)
            ->header('Content-Type', 'application/json');
        
    }

    public function test(){
        logg(__CLASS__.'::'.__FUNCTION__.': '.__LINE__);
        Redis::publish('telegram:send', json_encode(['chat_id'=>-4100451681, 'text'=>'This is a message, sent by redis emit at '.date('H:i:s')]));
        echo "done";
    }

    // public static function test() {
    //     Telegram::sendMessage([
    //         'chat_id' => -4100451681,
    //         'text' => 'This is a message, sent by redis emit at '.date('H:i:s')
    //     ]);
    //     logg('Telegram::sendMessage ' .$this->chat_id.' : '.$this->text);
    // }


}

